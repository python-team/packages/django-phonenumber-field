Source: django-phonenumber-field
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Tom Teichler <debdolph@tom-teichler.de>,
 Dominik George <natureshadow@debian.org>,
 Michael Fladischer <fladi@debian.org>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 pybuild-plugin-pyproject,
 python-django-doc,
 python3-all,
 python3-babel,
 python3-django,
 python3-djangorestframework,
 python3-doc,
 python3-phonenumbers,
 python3-pytest,
 python3-pytest-django,
 python3-setuptools,
 python3-setuptools-scm,
 python3-sphinx,
 python3-sphinx-rtd-theme,
Standards-Version: 4.6.2
Homepage: https://github.com/stefanfoulis/django-phonenumber-field
Vcs-Git: https://salsa.debian.org/python-team/packages/django-phonenumber-field.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/django-phonenumber-field
Rules-Requires-Root: no

Package: python-django-phonenumber-field-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Model and form field for normalised phone numbers (Documentation)
 A Django library which interfaces with python-phonenumbers to validate,
 pretty print and convert phone numbers.  python-phonenumbers is a port of
 Google's libphonenumber library, which powers Android's phone number
 handling.
 .
 Included are:
 .
  * PhoneNumber, a pythonic wrapper around python-phonenumbers' PhoneNumber
    class
  * PhoneNumberField, a model field
  * PhoneNumberField, a form field
  * PhoneNumberField, a serializer field
  * PhoneNumberPrefixWidget, a form widget for selecting a region code and
    entering a national number.  Requires the Babel package be installed.
  * PhoneNumberInternationalFallbackWidget, a form widget that uses national
    numbers unless an international number is entered.  A
    PHONENUMBER_DEFAULT_REGION setting needs to be added to your Django
    settings in order to know which national number format to recognize.
 .
 This package contains the documentation.

Package: python3-django-phonenumber-field
Architecture: all
Depends:
 python3-phonenumbers,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-django-phonenumber-field-doc,
Description: Model and form field for normalised phone numbers (Python 3 version)
 A Django library which interfaces with python-phonenumbers to validate,
 pretty print and convert phone numbers.  python-phonenumbers is a port of
 Google's libphonenumber library, which powers Android's phone number
 handling.
 .
 Included are:
 .
  * PhoneNumber, a pythonic wrapper around python-phonenumbers' PhoneNumber
    class
  * PhoneNumberField, a model field
  * PhoneNumberField, a form field
  * PhoneNumberField, a serializer field
  * PhoneNumberPrefixWidget, a form widget for selecting a region code and
    entering a national number.  Requires the Babel package be installed.
  * PhoneNumberInternationalFallbackWidget, a form widget that uses national
    numbers unless an international number is entered.  A
    PHONENUMBER_DEFAULT_REGION setting needs to be added to your Django
    settings in order to know which national number format to recognize.
